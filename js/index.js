function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}

var counter = 1;
var limit = 100;
           
function addInput(divName){
    if (counter == limit)  {
        alert("You have reached the limit of adding " + counter + " inputs");
    }
    else {
        var newdiv = document.createElement('div');
        newdiv.setAttribute("class", "group");
        newdiv.innerHTML = '<label for="pass" class="label">Subcategory ' + (counter + 1) + ":</label>"  + '<br><label for="user" class="label">Subcategory Code</label><input id="user" type="text" class="input" data-type="text"><br><label for="user" class="label">Subcategory Name</label><input id="user" type="password" class="input" data-type="password">';
        document.getElementById(divName).appendChild(newdiv);
        counter++;
    }
}